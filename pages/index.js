import React, {useState} from "react"
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'

export default () => {
    const [inputArea, setInputArea] = useState({})
    const [selectValueArea, setSelectValueArea] = useState('acre');
    const [resultArea, setResultArea] = useState({acre: 0, m2: 0, rai: 0})
    const [inputDistance, setInputDistance] = useState({})
    const [selectValueDistance, setSelectValueDistance] = useState('mile');
    const [resultDistance, setResultDistance] = useState({mile: 0, metre: 0, wa: 0})
    const [inputVolume, setInputVolume] = useState({})
    const [selectValueVolume, setSelectValueVolume] = useState('oz');
    const [resultVolume, setResultVolume] = useState({oz: 0, cm3: 0, ml: 0})
    const [inputSpeed, setInputSpeed] = useState({})
    const [selectValueSpeed, setSelectValueSpeed] = useState('mph');
    const [resultSpeed, setResultSpeed] = useState({mph: 0, ms: 0, kmh: 0})

    async function convertArea(type, value){
        if(type === 'acre'){
            let m2 = parseFloat(value * 4046.8564);
            let rai = parseFloat(value * 2.53);
            setResultArea({acre: value, m2: m2, rai: rai})
        }else if(type === 'm2'){
            let acre = parseFloat(value * 0.000247105381);
            let rai = parseFloat(value * 0.000625);
            setResultArea({acre: acre, m2: value, rai: rai})
        }else if(type === 'rai'){
            let acre = parseFloat(value * 0.395256916996);
            let m2 = parseFloat(value * 1600);
            setResultArea({acre: acre, m2: m2, rai: value})
        }
    }

    async function convertDistance(type, value){
        if(type === 'mile'){
            let metre = parseFloat(value * 1609.344);
            let wa = parseFloat(metre * 0.5);
            setResultDistance({mile: value, metre: metre, wa: wa})
        }else if(type === 'metre'){
            let mile = parseFloat(value * 0.000621371192237);
            let wa = parseFloat(value * 0.5);
            setResultDistance({mile: mile, metre: value, wa: wa})
        }else if(type === 'wa'){
            let metre = parseFloat(value * 2);
            let mile = parseFloat(metre * 0.000621371192237);
            setResultDistance({mile: mile, metre: metre, wa: value})
        }
    }

    async function convertVolume(type, value){
        if(type === 'oz'){
            let cm3 = parseFloat(value * 28.4131);
            let ml = parseFloat(value * 29.57353);
            setResultVolume({oz: value, cm3: cm3, ml: ml})
        }else if(type === 'cm3'){
            let oz = parseFloat(value * 0.033814022701843);
            let ml = parseFloat(value);
            setResultVolume({oz: oz, cm3: value, ml: ml})
        }else if(type === 'ml'){
            let oz = parseFloat(value * 0.033814022701843);
            let cm3 = parseFloat(value);
            setResultVolume({oz: oz, cm3: cm3, ml: value})
        }
    }

    async function convertSpeed(type, value){
        if(type === 'mph'){
            let ms = parseFloat(value * 0.44704);
            let kmh = parseFloat(value * 1.609344);
            setResultSpeed({mph: value, ms: ms, kmh: kmh})
        }else if(type === 'ms'){
            let mph = parseFloat(value * 2.2369);
            let kmh = parseFloat(value * 3.6);
            setResultSpeed({mph: mph, ms: value, kmh: kmh})
        }else if(type === 'kmh'){
            let mph = parseFloat(value * 0.621371192237);
            let ms = parseFloat(value * 0.277777);
            setResultSpeed({mph: mph, ms: ms, kmh: value})
        }
    }

    return (
        <div>
            <div>
                <h3> Area </h3>
                <TextField
                    size="small"
                    id="outlined-basic"
                    variant="outlined"
                    onChange={((e) => {
                        setInputArea({value: e.target.value});
                    })}/>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={selectValueArea}
                    label="Type"
                    onChange={(e) => {
                        setSelectValueArea(e.target.value);
                    }}
                >
                    <MenuItem value={'acre'}>acre</MenuItem>
                    <MenuItem value={'m2'}>m2</MenuItem>
                    <MenuItem value={'rai'}>rai(ไร่)</MenuItem>
                </Select>
                <Button
                    variant="contained"
                    color="primary"
                    onClick={() => convertArea(selectValueArea, inputArea.value)}
                    disabled={!inputArea.value}>
                    Convert
                </Button>
                <div>acre: {resultArea.acre}</div>
                <div>m2: {resultArea.m2}</div>
                <div>rai(ไร่): {resultArea.rai}</div>
            </div>

            <div>
                <h3> Distance </h3>
                <TextField
                    size="small"
                    id="outlined-basic"
                    variant="outlined"
                    onChange={((e) => {
                        setInputDistance({value: e.target.value});
                    })}/>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={selectValueDistance}
                    label="Type"
                    onChange={(e) => {
                        setSelectValueDistance(e.target.value);
                    }}
                >
                    <MenuItem value={'mile'}>mile</MenuItem>
                    <MenuItem value={'metre'}>metre</MenuItem>
                    <MenuItem value={'wa'}>wa(วา)</MenuItem>
                </Select>
                <Button
                    variant="contained"
                    color="primary"
                    onClick={() => convertDistance(selectValueDistance, inputDistance.value)}
                    disabled={!inputDistance.value}>
                    Convert
                </Button>
                <div>mile: {resultDistance.mile}</div>
                <div>metre: {resultDistance.metre}</div>
                <div>wa(วา): {resultDistance.wa}</div>
            </div>

            <div>
                <h3> Volume </h3>
                <TextField
                    size="small"
                    id="outlined-basic"
                    variant="outlined"
                    onChange={((e) => {
                        setInputVolume({value: e.target.value});
                    })}/>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={selectValueVolume}
                    label="Type"
                    onChange={(e) => {
                        setSelectValueVolume(e.target.value);
                    }}
                >
                    <MenuItem value={'oz'}>fl oz.</MenuItem>
                    <MenuItem value={'cm3'}>cm3</MenuItem>
                    <MenuItem value={'ml'}>mL</MenuItem>
                </Select>
                <Button
                    variant="contained"
                    color="primary"
                    onClick={() => convertVolume(selectValueVolume, inputVolume.value)}
                    disabled={!inputVolume.value}>
                    Convert
                </Button>
                <div>fl oz.: {resultVolume.oz}</div>
                <div>cm3: {resultVolume.cm3}</div>
                <div>mL: {resultVolume.ml}</div>
            </div>

            <div>
                <h3> Speed </h3>
                <TextField
                    size="small"
                    id="outlined-basic"
                    variant="outlined"
                    onChange={((e) => {
                        setInputSpeed({value: e.target.value});
                    })}/>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={selectValueSpeed}
                    label="Type"
                    onChange={(e) => {
                        setSelectValueSpeed(e.target.value);
                    }}
                >
                    <MenuItem value={'mph'}>mph</MenuItem>
                    <MenuItem value={'ms'}>m/s</MenuItem>
                    <MenuItem value={'kmh'}>km/h</MenuItem>
                </Select>
                <Button
                    variant="contained"
                    color="primary"
                    onClick={() => convertSpeed(selectValueSpeed, inputSpeed.value)}
                    disabled={!inputSpeed.value}>
                    Convert
                </Button>
                <div>mph: {resultSpeed.mph}</div>
                <div>m/s: {resultSpeed.ms}</div>
                <div>km/h: {resultSpeed.kmh}</div>
            </div>
        </div>
    )
}